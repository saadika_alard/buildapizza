/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('buildApizza')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
